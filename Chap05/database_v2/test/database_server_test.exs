defmodule DatabaseServerTest do
  use ExUnit.Case

  test "Get sent value" do
    pid = DatabaseServer.start()
    DatabaseServer.run_async(pid, 1)

    result = DatabaseServer.get_result()
    assert String.contains?(result, "1 result") == true
  end
end
