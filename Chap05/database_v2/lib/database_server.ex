defmodule DatabaseServer do
  @moduledoc """
  Documentation for `DatabaseServer`.
  """

  @doc """
  `start` creates a database server process.
  """
  @spec start() :: pid
  def start() do
    spawn(
      fn ->
        connection = :rand.uniform(1000)
        loop(connection)
      end
    )
  end

  @doc """
  `run_async` sends async messages to
    The database server process.
  """
  @spec run_async(pid, term) :: no_return
  def run_async(server_pid, query_def) do
    send(server_pid, {:run_query, self(), query_def})
  end

  @doc """
  `get_result` returns the oldest message in the queue
    Of the form {:query_result, term}. Or else,
    It times out.
  """
  @spec get_result() :: term
  def get_result() do
    receive do
      {:query_result, result} ->
        result
    after 5000 ->
      {:error, :timeout}
    end
  end

  defp loop(connection) do
    receive do
      {:run_query, caller, query_def} ->
        send(caller, {:query_result, run_query(connection, query_def)})
    end
    loop(connection)
  end

  defp run_query(connection, query_def) do
    Process.sleep(2000)
    "Connection #{connection}: #{query_def} result"
  end
end
