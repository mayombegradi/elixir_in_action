defmodule DatabaseServerTest do
  use ExUnit.Case

  test "Get sent value" do
    pid = DatabaseServer.start()
    DatabaseServer.run_async(pid, 1)

    assert DatabaseServer.get_result() == "1 result"
  end
end
