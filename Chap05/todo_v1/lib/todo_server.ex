defmodule TodoServer do
  @moduledoc """
  Documentation for `TodoServer`.
  """

  @spec start() :: pid
  def start() do
    pid = spawn(fn -> loop(TodoList.new([])) end)
    Process.register(pid, __MODULE__)
  end

  @spec add_entry(map) :: no_return
  def add_entry(entry) do
    send(__MODULE__, {:add_entry, entry})
  end

  @spec update_entry(map) :: no_return
  def update_entry(entry) do
    send(__MODULE__, {:update_entry, entry})
  end

  @spec entries(Date.t) :: list | {:error, :timeout}
  def entries(date) do
    send(__MODULE__, {:entries, self(), date})
    receive do
      {:entries, entries} ->
        entries
      after 5000 ->
        {:error, :timeout}
    end
  end

  defp loop(todo_list) do
    receive do
      message ->
        loop(process_message(todo_list, message))
    end
  end

  defp process_message(todo_list, {:add_entry, entry}) do
    TodoList.add_entry(todo_list, entry)
  end
  defp process_message(todo_list, {:update_entry, entry}) do
    TodoList.update_entry(todo_list, entry)
  end
  defp process_message(todo_list, {:entries, caller, date}) do
    entries = TodoList.entries(todo_list, date)
    send(caller, {:entries, entries})
    todo_list
  end
end
