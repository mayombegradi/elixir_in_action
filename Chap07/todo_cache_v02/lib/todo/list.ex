defmodule Todo.List do
  @moduledoc """
  Documentation for `Todo.List`.
  """

  defstruct auto_id: 1, entries: %{}

  @spec new([map]) :: %Todo.List{}
  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %Todo.List{},
      fn entry, todo_list_acc ->
        add_entry(todo_list_acc, entry)
      end
    )
  end

  @spec add_entry(%Todo.List{}, map) :: %Todo.List{}
  def add_entry(todo_list=%Todo.List{ auto_id: id, entries: entries }, entry) do
    %Todo.List{ todo_list |
      entries: put_in(entries, [id], Map.put(entry, :id, id)),
      auto_id: id+1 }
  end

  @spec entries(%Todo.List{}, Date.t) :: list
  def entries(%Todo.List{ entries: entries }, key) do
    for {_id, %{ date: date }=entry} when date == key <- entries do
      entry
    end
  end

  @spec update_entry(%Todo.List{}, map) :: %Todo.List{}
  def update_entry(todo_list=%Todo.List{ entries: entries }, new_entry=%{ id: id }) do
    %Todo.List{ todo_list | entries: put_in(entries, [id], new_entry) }
  end

  @spec update_entry(%Todo.List{}, integer, fun) :: %Todo.List{}
  def update_entry(todo_list=%Todo.List{ entries: entries }, entry_id, lambda) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry} ->
        new_entry = %{ id: ^entry_id } = lambda.(old_entry)
        %Todo.List{ todo_list | entries: put_in(entries, [entry_id], new_entry) }
    end
  end
end
