defmodule Todo.Server do
  @moduledoc """
  Documentation for `Todo.Server`.
  """
  use GenServer

  @spec start(binary) :: {:ok, pid}
  def start(name) do
    GenServer.start(__MODULE__, name)
  end

  @spec entries(pid, Date.t) :: [map]
  def entries(server, date) do
    GenServer.call(server, {:entries, date})
  end

  @spec add_entry(pid, map) :: no_return
  def add_entry(server, entry) do
    GenServer.cast(server, {:add_entry, entry})
  end

  @spec update_entry(pid, map) :: no_return
  def update_entry(server, entry) do
    GenServer.cast(server, {:update_entry, entry})
  end

  @impl GenServer
  def init(name) do
    {:ok, {name, Todo.Database.get(name) || Todo.List.new([])}}
  end

  @impl GenServer
  def handle_call({:entries, date}, _from, {name, todo_list}) do
    entries = Todo.List.entries(todo_list, date)
    {:reply, entries, {name, todo_list}}
  end

  @impl GenServer
  def handle_cast({:add_entry, entry}, {name, todo_list}) do
    new_list = Todo.List.add_entry(todo_list, entry)
    Todo.Database.store(name, new_list)
    {:noreply, {name, new_list}}
  end
  def handle_cast({:update_entry, entry}, todo_list) do
    {:noreply, Todo.List.update_entry(todo_list, entry)}
  end
end
