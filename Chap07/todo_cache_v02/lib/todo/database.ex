defmodule Todo.Database do
  use GenServer
  @db_folder "./persist"

  @spec start :: {:ok, pid}
  def start, do: GenServer.start(__MODULE__, nil, name: __MODULE__)

  @spec get(term) :: term
  def get(key), do: GenServer.call(__MODULE__, {:get, key})

  @spec store(term, term) :: no_return
  def store(key, data), do: GenServer.cast(__MODULE__, {:store, key, data})

  @impl GenServer
  def init(_args) do
    File.mkdir_p!(@db_folder)
    {:ok, nil}
  end

  @impl GenServer
  def handle_call({:get, key}, _from, state) do
    data =
      case File.read(file_name(key)) do
        {:ok, contents} -> :erlang.binary_to_term(contents)
        _error -> nil
      end

    {:reply, data, state}
  end

  @impl GenServer
  def handle_cast({:store, key, data}, state) do
    key
    |> file_name()
    |> File.write!(:erlang.term_to_binary(data))

    {:noreply, state}
  end

  defp file_name(key) do
    Path.join(@db_folder, to_string(key))
  end

end
