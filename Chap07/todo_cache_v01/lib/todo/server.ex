defmodule Todo.Server do
  @moduledoc """
  Documentation for `Todo.Server`.
  """
  use GenServer

  @spec start :: {:ok, pid}
  def start do
    GenServer.start(__MODULE__, nil)
  end

  @spec entries(pid, Date.t) :: [map]
  def entries(server, date) do
    GenServer.call(server, {:entries, date})
  end

  @spec add_entry(pid, map) :: no_return
  def add_entry(server, entry) do
    GenServer.cast(server, {:add_entry, entry})
  end

  @spec update_entry(pid, map) :: no_return
  def update_entry(server, entry) do
    GenServer.cast(server, {:update_entry, entry})
  end

  @impl GenServer
  def init(nil) do
    {:ok, Todo.List.new([])}
  end

  @impl GenServer
  def handle_call({:entries, date}, _from, todo_list) do
    entries = Todo.List.entries(todo_list, date)
    {:reply, entries, todo_list}
  end

  @impl GenServer
  def handle_cast({:add_entry, entry}, todo_list) do
    {:noreply, Todo.List.add_entry(todo_list, entry)}
  end
  def handle_cast({:update_entry, entry}, todo_list) do
    {:noreply, Todo.List.update_entry(todo_list, entry)}
  end
end
