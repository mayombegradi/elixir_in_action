defmodule Todo.Cache do
  use GenServer

  @spec start :: {:ok, pid}
  def start, do: GenServer.start(__MODULE__, nil)

  @spec server_process(pid, term) :: term
  def server_process(cache_id, todo_list_name) do
    GenServer.call(cache_id, {:server_process, todo_list_name})
  end

  @impl GenServer
  def init(_args), do: {:ok, %{}}

  @impl GenServer
  def handle_call({:server_process, todo_list_name}, _from, todo_servers) do
    case Map.fetch(todo_servers, todo_list_name) do
      {:ok, todo_server} ->
        {:reply, todo_server, todo_servers}
      :error ->
        {:ok, new_server} = Todo.Server.start()
        todo_servers =
          Map.put(
            todo_servers,
            todo_list_name,
            new_server
          )
        {:reply, new_server, todo_servers}
    end
  end
end
