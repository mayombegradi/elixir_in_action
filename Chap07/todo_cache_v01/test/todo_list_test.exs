defmodule TodoListTest do
  use ExUnit.Case

  test "Create a new todo_list" do
    assert Todo.List.new() == %Todo.List{}
  end

  test "Create a new todo_list from a list of entries" do
    entries = [
      %{date: ~D[2018-12-19], title: "Dentist"},
      %{date: ~D[2018-12-20], title: "Shopping"},
      %{date: ~D[2018-12-19], title: "Movies"}
    ]

    todo_list = Todo.List.new(entries)

    assert todo_list ==
             %Todo.List{
               auto_id: 4,
               entries: %{
                 1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
                 2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
                 3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
               }
             }
  end

  test "Add entries to todo_list" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Movies"})

    assert todo_list ==
             %Todo.List{
               auto_id: 4,
               entries: %{
                 1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
                 2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
                 3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
               }
             }
  end

  test "Get entries by date" do
    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Movies"})

    assert Todo.List.entries(todo_list, ~D[2018-12-19]) ==
             [
               %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
               %{date: ~D[2018-12-19], id: 3, title: "Movies"}
             ]
  end

  test "Update entry" do
    lambda = fn entry -> Map.put(entry, :title, "Go to the gym") end

    todo_list =
      Todo.List.new()
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
      |> Todo.List.add_entry(%{date: ~D[2018-12-19], title: "Movies"})
      |> Todo.List.update_entry(1, lambda)

    assert todo_list ==
             %Todo.List{
               auto_id: 4,
               entries: %{
                 1 => %{date: ~D[2018-12-19], id: 1, title: "Go to the gym"},
                 2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
                 3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
               }
             }
  end
end
