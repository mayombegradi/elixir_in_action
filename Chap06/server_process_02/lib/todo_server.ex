defmodule TodoServer do
  @moduledoc """
  Documentation for `TodoServer`.
  """

  @spec start() :: pid
  def start() do
    ServerProcess.start(__MODULE__)
  end

  @spec add_entry(pid, map) :: no_return
  def add_entry(server_pid, entry) do
    ServerProcess.cast(server_pid, {:add_entry, entry})
  end

  @spec update_entry(pid, map) :: no_return
  def update_entry(server_pid, entry) do
    ServerProcess.cast(server_pid, {:update_entry, entry})
  end

  @spec entries(pid, Date.t) :: list | {:error, :timeout}
  def entries(server_pid, date) do
    ServerProcess.call(server_pid, {:entries, date})
  end

  def init() do
    TodoList.new([])
  end

  def handle_call({:entries, date}, todo_list) do
    entries = TodoList.entries(todo_list, date)
    {entries, todo_list}
  end

  def handle_cast({:add_entry, entry}, todo_list) do
    TodoList.add_entry(todo_list, entry)
  end
  def handle_cast({:update_entry, entry}, todo_list) do
    TodoList.update_entry(todo_list, entry)
  end

end
