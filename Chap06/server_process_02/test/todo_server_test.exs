defmodule TodoServerTest do
  use ExUnit.Case

  test "Get empty entry list" do
    pid = TodoServer.start()
    assert TodoServer.entries(pid, ~D[2018-12-19]) == []
  end

  test "Get entries" do
    pid = TodoServer.start()
    TodoServer.add_entry(pid, %{date: ~D[2018-12-19], title: "Dentist"})
    TodoServer.add_entry(pid, %{date: ~D[2018-12-20], title: "Shopping"})
    TodoServer.add_entry(pid, %{date: ~D[2018-12-19], title: "Movies"})

    assert TodoServer.entries(pid, ~D[2018-12-19]) ==
      [
        %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
        %{date: ~D[2018-12-19], id: 3, title: "Movies"}
      ]
  end
end
