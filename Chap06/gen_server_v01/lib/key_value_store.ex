defmodule KeyValueStore do
  @moduledoc """
  Documentation for `KeyValueStore`.
  """
  use GenServer

  @spec start :: {:ok, pid}
  def start do
    GenServer.start(__MODULE__, nil)
  end

  @spec get(pid, term) :: term
  def get(pid, key) do
    GenServer.call(pid, {:get, key})
  end

  @spec put(pid, term, term) :: no_return
  def put(pid, key, value) do
    GenServer.cast(pid, {:put, key, value})
  end

  @impl GenServer
  def init(_args) do
    {:ok, %{}}
  end

  @impl GenServer
  def handle_call({:get, key}, _from, state) do
    {:reply, Map.get(state, key), state}
  end

  @impl GenServer
  def handle_cast({:put, key, value}, state) do
    {:noreply, Map.put(state, key, value)}
  end

  @impl GenServer
  def handle_info(message, state) do
    IO.puts("Message: #{inspect(message)}")
    {:noreply, state}
  end
end
