defmodule KeyValueStoreTest do
  use ExUnit.Case

  test "Get element" do
    {:ok, pid} = KeyValueStore.start

    assert KeyValueStore.get(pid, :some_key) == nil

    KeyValueStore.put(pid, :some_key, :some_value)
    assert KeyValueStore.get(pid, :some_key) == :some_value
  end
end
