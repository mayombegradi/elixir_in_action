defmodule TodoServerTest do
  use ExUnit.Case

  test "Get entries" do
    {:ok, _pid} = TodoServer.start()
    TodoServer.add_entry(%{date: ~D[2018-12-19], title: "Dentist"})
    TodoServer.add_entry(%{date: ~D[2018-12-20], title: "Shopping"})
    TodoServer.add_entry(%{date: ~D[2018-12-19], title: "Movies"})

    assert TodoServer.entries(~D[2018-12-19]) ==
      [
        %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
        %{date: ~D[2018-12-19], id: 3, title: "Movies"}
      ]
  end
end
