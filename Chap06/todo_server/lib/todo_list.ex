defmodule TodoList do
  @moduledoc """
  Documentation for `TodoList`.
  """

  defstruct auto_id: 1, entries: %{}

  @spec new([map]) :: %TodoList{}
  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %TodoList{},
      fn entry, todo_list_acc ->
        add_entry(todo_list_acc, entry)
      end
    )
  end

  @spec add_entry(%TodoList{}, map) :: %TodoList{}
  def add_entry(todo_list=%TodoList{ auto_id: id, entries: entries }, entry) do
    %TodoList{ todo_list |
      entries: put_in(entries, [id], Map.put(entry, :id, id)),
      auto_id: id+1 }
  end

  @spec entries(%TodoList{}, Date.t) :: list
  def entries(%TodoList{ entries: entries }, key) do
    for {_id, %{ date: date }=entry} when date == key <- entries do
      entry
    end
  end

  @spec update_entry(%TodoList{}, map) :: %TodoList{}
  def update_entry(todo_list=%TodoList{ entries: entries }, new_entry=%{ id: id }) do
    %TodoList{ todo_list | entries: put_in(entries, [id], new_entry) }
  end

  @spec update_entry(%TodoList{}, integer, fun) :: %TodoList{}
  def update_entry(todo_list=%TodoList{ entries: entries }, entry_id, lambda) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry} ->
        new_entry = %{ id: ^entry_id } = lambda.(old_entry)
        %TodoList{ todo_list | entries: put_in(entries, [entry_id], new_entry) }
    end
  end
end
