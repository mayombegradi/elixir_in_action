defmodule TodoServer do
  @moduledoc """
  Documentation for `TodoServer`.
  """
  use GenServer

  @spec start :: {:ok, pid}
  def start do
    GenServer.start(__MODULE__, nil, name: __MODULE__)
  end

  @spec entries(Date.t) :: [map]
  def entries(date) do
    GenServer.call(__MODULE__, {:entries, date})
  end

  @spec add_entry(map) :: no_return
  def add_entry(entry) do
    GenServer.cast(__MODULE__, {:add_entry, entry})
  end

  @spec update_entry(map) :: no_return
  def update_entry(entry) do
    GenServer.cast(__MODULE__, {:update_entry, entry})
  end

  @impl GenServer
  def init(nil) do
    {:ok, TodoList.new([])}
  end

  @impl GenServer
  def handle_call({:entries, date}, _from, todo_list) do
    entries = TodoList.entries(todo_list, date)
    {:reply, entries, todo_list}
  end

  @impl GenServer
  def handle_cast({:add_entry, entry}, todo_list) do
    {:noreply, TodoList.add_entry(todo_list, entry)}
  end
  def handle_cast({:update_entry, entry}, todo_list) do
    {:noreply, TodoList.update_entry(todo_list, entry)}
  end
end
