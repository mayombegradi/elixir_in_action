defmodule KeyValueStore do

  @type state :: map

  @type key :: term
  @type value :: term
  @type call_request :: {:get, key}
  @type cast_request :: {:put, key, value}

  @spec start :: pid
  def start do
    ServerProcess.start(__MODULE__)
  end

  @spec put(pid, key, value) :: no_return
  def put(pid, key, value) do
    ServerProcess.cast(pid, {:put, key, value})
  end

  @spec get(pid, key) :: term
  def get(pid, key) do
    ServerProcess.call(pid, {:get, key})
  end

  @spec init() :: %{}
  def init(), do: %{}

  @spec handle_call(call_request, state) :: term
  def handle_call({:get, key}, state) do
    {Map.get(state, key), state}
  end

  @spec handle_cast(cast_request, state) :: state
  def handle_cast({:put, key, value}, state) do
    Map.put(state, key, value)
  end
end
