defmodule KeyValueStoreTest do
  use ExUnit.Case

  test "Get element" do
    {:ok, _pid} = KeyValueStore.start

    assert KeyValueStore.get(:some_key) == nil

    KeyValueStore.put(:some_key, :some_value)
    assert KeyValueStore.get(:some_key) == :some_value
  end
end
