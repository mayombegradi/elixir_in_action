defmodule Todo.List do
  defstruct auto_id: 1, entries: %{}

  @spec new(any) :: any
  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %Todo.List{},
      &add_entry(&2, &1)
    )
  end

  @spec size(atom | %{:entries => map, optional(any) => any}) ::
        non_neg_integer
  def size(todo_list) do
    Kernel.map_size(todo_list.entries)
  end

  @spec add_entry(%Todo.List{}, map) :: %Todo.List{}
  def add_entry(todo_list, entry) do
    entry = Map.put(entry, :id, todo_list.auto_id)
    new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)

    %Todo.List{todo_list | entries: new_entries, auto_id: todo_list.auto_id + 1}
  end

  @spec entries(%Todo.List{}, Date.t) :: list
  def entries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_key, entry} -> entry.date == date end)
    |> Enum.map(fn {_id, entry} -> entry end)
  end

  @spec update_entry(%Todo.List{}, map) :: %Todo.List{}
  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _element -> new_entry end)
  end

  @spec update_entry(%Todo.List{}, integer, fun) :: %Todo.List{}
  def update_entry(todo_list, entry_id, updater_fun) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list

      {:ok, old_entry} ->
        new_entry = updater_fun.(old_entry)
        new_entries = Map.put(todo_list.entries, new_entry.id, new_entry)
        %Todo.List{todo_list | entries: new_entries}
    end
  end

  @spec delete_entry(%Todo.List{}, integer) :: %Todo.List{}
  def delete_entry(todo_list, entry_id) do
    %Todo.List{todo_list | entries: Map.delete(todo_list.entries, entry_id)}
  end
end
