defmodule Echo do
  @moduledoc """
  Documentation for `Echo`.
  """

  use GenServer

  @spec start_link(term) :: {:ok, pid}
  def start_link(id) do
    GenServer.start_link(__MODULE__, nil, name: via_tuple(id))
  end

  @spec call(term, term) :: term
  def call(id, some_request) do
    GenServer.call(via_tuple(id), some_request)
  end

  @impl GenServer
  def init(_args) do
    {:ok, nil}
  end

  @impl GenServer
  def handle_call(some_request, _from, state) do
    {:reply, some_request, state}
  end

  defp via_tuple(id) do
    {:via, Registry, {:my_registry, {__MODULE__, id}}}
  end
end
