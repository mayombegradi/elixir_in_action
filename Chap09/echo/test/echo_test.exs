defmodule EchoTest do
  use ExUnit.Case

  test "call echo request" do
    Registry.start_link(name: :my_registry, keys: :unique)
    Echo.start_link("server one")
    assert :some_request == Echo.call("server one", :some_request)
  end
end
