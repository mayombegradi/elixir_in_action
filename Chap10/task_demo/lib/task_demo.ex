defmodule TaskDemo do
  @moduledoc """
  Documentation for `TaskDemo`.
  """

  @doc """
  ## Example

      iex> TaskDemo.get_results()
      ["1 result", "2 result", "3 result", "4 result", "5 result"]
  """
  def get_results() do
    get_tasks()
    |> Enum.map(&Task.await/1)
  end

  defp get_tasks() do
    Enum.map(1..5, &Task.async(fn -> run_query(&1) end))
  end

  defp run_query(query_def) do
    Process.sleep(2000)
    "#{query_def} result"
  end
end
