defmodule TaskDemoStream do
  def get_results() do
    Process.flag(:trap_exit, true)
    get_tasks()
    |> Enum.to_list()
  end

  defp get_tasks() do
    Task.async_stream(1..10, fn element -> run_query(element) end)
  end

  defp run_query(query_def) do
    Process.sleep(2000)
    case :rand.uniform(10) do
      value when rem(value, 2) == 0 -> exit(:tired)
      _other -> "#{query_def} result"
    end
  end
end
