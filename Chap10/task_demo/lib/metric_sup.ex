defmodule MetricSup do
  def start_link() do
    Supervisor.start_link([Metrics], strategy: :one_for_one)
  end
end
