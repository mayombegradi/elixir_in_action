defmodule AgentDemo do
  @moduledoc """
  Documentation for `AgentDemo`.
  """

  def start_link(name, age) do
    Agent.start_link(fn -> %{name: name, age: age} end)
  end

  def get_state(agent) do
    Agent.get(agent, fn state -> state end)
  end

  def update_state(agent, {:age, new_age}) when is_integer(new_age) do
    Agent.update(agent, fn state -> %{ state | age: new_age } end)
  end
  def update_state(agent, {:name, new_name}) when is_binary(new_name) do
    Agent.update(agent, fn state -> %{ state | name: new_name } end)
  end
end
