defmodule EtsKeyValue do
  @moduledoc """
  Documentation for `EtsKeyValue`.
  """

  use GenServer

  @spec start_link() :: {:ok, pid}
  def start_link do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @spec put(term, term) :: no_return
  def put(key, value) do
    :ets.insert(__MODULE__, {key, value})
  end

  @spec get(term) :: nil | term
  def get(key) do
    case :ets.lookup(__MODULE__, key) do
      [{^key, value}] -> value
      [] -> nil
    end
  end

  @impl GenServer
  def init(nil) do
    :ets.new(
      __MODULE__,
      [:named_table, :public, write_concurrency: true]
    )

    {:ok, nil}
  end

  @impl GenServer
  def handle_call(_message, _from, state) do
    {:reply, :ok, state}
  end

  @impl GenServer
  def handle_cast(_message, state) do
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(_info, state) do
    {:noreply, state}
  end
end
