defmodule FractionTest do
  use ExUnit.Case

  test "new fraction" do
    assert Fraction.new(1, 2) == %Fraction{denominator: 2, numerator: 1}
  end

  test "get value" do
    value =
      Fraction.new(1, 2)
      |> Fraction.value()

    assert value == 0.5
  end

  test "add 2 fractions" do
    value =
      Fraction.add(Fraction.new(1, 2), Fraction.new(1, 4))
      |> Fraction.value()

    assert value == 0.75
  end
end
