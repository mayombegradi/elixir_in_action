defmodule Fraction do
  @enforce_keys [:denominator]
  defstruct [:numerator, :denominator]

  @spec new(number, number) :: %Fraction{}
  def new(num \\0, denom) do
    %Fraction{
      numerator: num,
      denominator: denom
    }
  end

  @spec value(%Fraction{}) :: float
  def value(fraction=%Fraction{}) do
    fraction.numerator / fraction.denominator
  end

  @spec add(%Fraction{}, %Fraction{}) :: %Fraction{}
  def add(frac_1=%Fraction{}, frac_2=%Fraction{}) do
    new(
      frac_1.numerator * frac_2.denominator +
      frac_2.numerator * frac_1.denominator,
      frac_1.denominator * frac_2.denominator
    )
  end
end
