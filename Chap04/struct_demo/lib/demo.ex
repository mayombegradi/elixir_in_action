defmodule Demo do
  @moduledoc """
  Documentation for `Demo`.
  """

  @spec hello() :: :world
  def hello do
    :world
  end
end
