defmodule TodoList do
  @moduledoc """
  Documentation for `TodoList`.
  """

  defstruct auto_id: 1, entries: %{}

  @spec new() :: %TodoList{}
  def new(), do: %TodoList{}

  @spec add_entry(%TodoList{}, map) :: %TodoList{}
  def add_entry(todo_list=%TodoList{ auto_id: id, entries: entries }, entry) do
    entry = Map.put(entry, :id, id)
    new_entries = Map.put(entries, id, entry)

    %TodoList{ todo_list |
      entries: new_entries,
      auto_id: id+1 }
  end

  @spec entries(%TodoList{}, Date.t) :: list
  def entries(%TodoList{ entries: entries }, key) do
    entries = get_entries(entries)
    for entry = %{ date: date } when date == key <- entries, do: entry
  end

  @spec update_entry(%TodoList{}, map) :: %TodoList{}
  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _arg -> new_entry end)
  end

  @spec update_entry(%TodoList{}, integer, fun) :: %TodoList{}
  def update_entry(todo_list=%TodoList{ entries: entries }, entry_id, lambda) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry} ->
        new_entry = %{ id: ^entry_id } = lambda.(old_entry)
        new_entries = Map.put(entries, new_entry.id, new_entry)
        %TodoList{ todo_list | entries: new_entries }
    end
  end

  defp get_entries(entries) do
    for {_id, entry} <- Map.to_list(entries), do: entry
  end
end
