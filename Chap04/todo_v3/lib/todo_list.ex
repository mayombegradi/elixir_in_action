defmodule TodoList do
  @moduledoc """
  Documentation for `TodoList`.
  """

  defstruct auto_id: 1, entries: %{}

  @spec new([map]) :: %TodoList{}
  def new(entries \\ []) do
    Enum.reduce(
      entries,
      %TodoList{},
      fn entry, todo_list_acc ->
        add_entry(todo_list_acc, entry)
      end
    )
  end

  @spec add_entry(%TodoList{}, map) :: %TodoList{}
  def add_entry(todo_list=%TodoList{ auto_id: id, entries: entries }, entry) do
    %TodoList{ todo_list |
      entries: put_in(entries, [id], Map.put(entry, :id, id)),
      auto_id: id+1 }
  end

  @spec entries(%TodoList{}, Date.t) :: list
  def entries(%TodoList{ entries: entries }, key) do
    for {_id, %{ date: date }=entry} when date == key <- entries do
      entry
    end
  end

  @spec update_entry(%TodoList{}, map) :: %TodoList{}
  def update_entry(todo_list=%TodoList{ entries: entries }, new_entry=%{ id: id }) do
    %TodoList{ todo_list | entries: put_in(entries, [id], new_entry) }
  end

  @spec update_entry(%TodoList{}, integer, fun) :: %TodoList{}
  def update_entry(todo_list=%TodoList{ entries: entries }, entry_id, lambda) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry} ->
        new_entry = %{ id: ^entry_id } = lambda.(old_entry)
        %TodoList{ todo_list | entries: put_in(entries, [entry_id], new_entry) }
    end
  end


  defmodule CsvImporter do
    @spec import!(Path.t) :: %TodoList{}
    def import!(file_path) do
      File.stream!(file_path)
      |> Stream.map(&String.replace(&1, "\n",""))
      |> Stream.map(&String.split(&1, ","))
      |> Stream.map(&data_transform/1)
      |> Enum.to_list()
      |> TodoList.new()
    end

    defp data_transform([date, title]) do
      %{ date: to_elixir_date!(date), title: title }
    end

    defp to_elixir_date!(date) do
      String.split(date, "/")
      |> Enum.map(&String.to_integer/1)
      |> List.to_tuple()
      |> Date.from_erl!()
    end
  end

  defimpl String.Chars, for: TodoList do
    def to_string(%TodoList{ auto_id: id, entries: entries}) do
      inspect(%{ __struct__: TodoList, auto_id: id, entries: entries })
    end
  end

  defimpl Collectable, for: TodoList do
    def into(original) do
      {original, &into_callback/2}
    end

    defp into_callback(_todo_list, :halt) do
      :ok
    end
    defp into_callback(todo_list, :done) do
      todo_list
    end
    defp into_callback(todo_list, {:cont, entry}) do
      TodoList.add_entry(todo_list, entry)
    end
  end

end
