defmodule TodoListCsvImporterTest do
  use ExUnit.Case
  @path_file "/home/ericson/Tut/Elixir/InAction/Chap04/todo_v3/priv/todos.csv"

  test "Import entries from a file" do
    todo_list = TodoList.CsvImporter.import!(@path_file)
    assert todo_list ==
      %TodoList{
        auto_id: 4,
        entries: %{
          1 => %{date: ~D[2018-12-19], id: 1, title: "Dentist"},
          2 => %{date: ~D[2018-12-20], id: 2, title: "Shopping"},
          3 => %{date: ~D[2018-12-19], id: 3, title: "Movies"}
        }
      }
  end
end
