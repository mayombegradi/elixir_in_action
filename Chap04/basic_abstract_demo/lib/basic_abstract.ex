defmodule BasicAbstract do

  @spec mapset_demo() :: no_return
  def mapset_demo() do
    days =
      MapSet.new()
      |> MapSet.put(:monday)
      |> MapSet.put(:tuesday)

    day = random_day()
    is_member? = MapSet.member?(days, day)

    IO.puts("Days: #{inspect(days)}")
    IO.puts("Is `days` a map? #{is_map(days)}")
    IO.puts("Is #{day} a member of #{inspect(days)}? #{is_member?}")
  end

  defp random_day() do
    [:monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday]
    |> Enum.at(:rand.uniform(7) - 1)
  end
end
