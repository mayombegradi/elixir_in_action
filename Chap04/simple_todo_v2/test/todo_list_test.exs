defmodule TodoListTest do
  use ExUnit.Case

  test "create a new todo list" do
    assert TodoList.new() == %{}
  end

  test "add entry to a todo list" do
    date = ~D[2022-12-22]

    todo_list =
      TodoList.new()
      |> TodoList.add_entry(date, "Go to the gym")
      |> TodoList.add_entry(date, "Read Python")

    assert todo_list == %{~D[2022-12-22] => ["Read Python", "Go to the gym"]}
  end

  test "get entries from todo list with correct date" do
    date = ~D[2022-12-22]
    todo_list =
      TodoList.new()
      |> TodoList.add_entry(date, "Go to the gym")
      |> TodoList.add_entry(date, "Read Python")

    assert TodoList.entries(todo_list, date) == ["Read Python", "Go to the gym"]
  end

  test "get entries from todo list with wrong date" do
    date = ~D[2022-12-22]
    todo_list =
      TodoList.new()
      |> TodoList.add_entry(date, "Go to the gym")
      |> TodoList.add_entry(date, "Read Python")

    assert TodoList.entries(todo_list, ~D[2022-12-23]) == []
  end

  test "get entries that are due today" do
    date = Date.from_erl!(:erlang.date())
    todo_list =
      TodoList.new()
      |> TodoList.add_entry(date, "Go to the gym")
      |> TodoList.add_entry(date, "Read Python")

    assert TodoList.due_today(todo_list) == ["Read Python", "Go to the gym"]
  end
end
