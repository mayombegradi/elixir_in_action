defmodule TodoList do

  @opaque todo_list :: %{}

  @spec new() :: todo_list
  def new(), do: MultiDict.new()

  @spec add_entry(todo_list, Date.t, binary) :: todo_list
  def add_entry(todo_list, date, title) do
    MultiDict.add(todo_list, date, title)
  end

  @spec entries(todo_list, Date.t) :: list
  def entries(todo_list, date) do
    MultiDict.get(todo_list, date)
  end

  @spec due_today(todo_list) :: list
  def due_today(todo_list) do
    today = Date.from_erl!(:erlang.date())
    entries(todo_list, today)
  end
end
