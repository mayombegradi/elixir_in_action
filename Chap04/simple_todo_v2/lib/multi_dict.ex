defmodule MultiDict do

  @spec new() :: %{}
  def new(), do: %{}

  @spec add(%{}, term, term) :: %{}
  def add(dict, key, value) do
    Map.update(dict, key, [value], &[value|&1])
  end

  @spec get(%{}, term) :: term
  def get(dict, key) do
    Map.get(dict, key, [])
  end
end
