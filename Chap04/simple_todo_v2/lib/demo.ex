defmodule Demo do

  @spec hello() :: :world
  def hello do
    :world
  end
end
