defmodule TodoList do

  @opaque todo_list :: %{}

  @doc """
  Creates an empty todo_list.

  ## Example

      iex> TodoList.new()
      %{}
  """
  @spec new() :: todo_list
  def new(), do: %{}

  @doc """
  Adds entries to `todo_list`.

  ## Examples

      iex> todo_list = TodoList.new(); date1 = ~D[2022-12-22]; date2 = ~D[2022-12-23]
      ~D[2022-12-23]

      iex> todo_list = TodoList.add_entry(todo_list, date1, "Go to the gym")
      %{~D[2022-12-22] => ["Go to the gym"]}

      iex> todo_list = TodoList.add_entry(todo_list, date2, "Watch AWS tutorial")
      %{~D[2022-12-22] => ["Go to the gym"], ~D[2022-12-23] => ["Watch AWS tutorial"]}

  """
  @spec add_entry(todo_list, Date.t, binary) :: todo_list
  def add_entry(todo_list, date, title) do
    Map.update(
      todo_list, date, [title],
      fn titles -> [title|titles] end)
  end

  @doc """
  Retrieves entries based on key from `todo_list`.

  ## Examples

      iex> date1 = ~D[2022-12-22]; date2 = ~D[2022-12-23]
      ~D[2022-12-23]

      iex> todo_list = %{~D[2022-12-22] => ["Go to the gym"], ~D[2022-12-23] => ["Watch AWS tutorial"]}
      %{~D[2022-12-22] => ["Go to the gym"], ~D[2022-12-23] => ["Watch AWS tutorial"]}

      iex> TodoList.entries(todo_list, date1)
      ["Go to the gym"]

      iex> TodoList.entries(todo_list, ~D[2022-12-26])
      []
  """
  @spec entries(todo_list, Date.t) :: list
  def entries(todo_list, date), do: Map.get(todo_list, date, [])
end
