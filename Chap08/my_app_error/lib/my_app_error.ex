defmodule MyAppError do
  @moduledoc """
  Documentation for `MyAppError`.
  """
  defexception [:message]

  @impl true
  def exception(value) do
    msg = "did not get what was expected, got: #{inspect(value)}"
    %MyAppError{message: msg}
  end

end
