defmodule DefexceptionDemo do
  defexception [:message]

  @impl true
  def exception(value) do
    msg = "did not get what was expected, got: #{inspect(value)}"
    %DefexceptionDemo{message: msg}
  end

  def demo() do
    try do
      raise(__MODULE__, :my_bad)
    catch :error, value -> IO.puts("#{inspect(value)}")
    end
  end
end
