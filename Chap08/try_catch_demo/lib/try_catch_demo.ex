defmodule TryCatchDemo do
  @moduledoc """
  Documentation for `TryCatchDemo`.
  """

  @doc """
  Example

      iex> TryCatchDemo.demo_1(fn -> raise("Something went wrong") end)
      Error
        :error
        %RuntimeError{message: "Something went wrong"}
      :ok

      iex> TryCatchDemo.demo_1(fn -> throw("Thrown value") end)
      Error
        :throw
        "Thrown value"
      :ok

      iex> TryCatchDemo.demo_1(fn -> exit("I'm done") end)
      Error
        :exit
        "I'm done"
      :ok
  """
  def demo_1(fun) do
    try_helper =
      fn ->
        try do
          fun.()
          IO.puts("No error.")
        catch type, value ->
          IO.puts("Error\n #{inspect(type)}\n #{inspect(value)}")
        end
      end

    try_helper.()
  end

  def demo_2(value) do
    try do
      case value do
        :exit  -> exit("It's an exit")
        :throw -> throw("It's a throw")
        :error -> raise("It's an error")
      end
    catch
      :exit,  value -> IO.puts("#{inspect(value)}")
      :throw, value -> IO.puts("#{inspect(value)}")
      :error, value -> IO.puts("#{inspect(value)}")
    after
      IO.puts("Error handled")
    end
  end
end
