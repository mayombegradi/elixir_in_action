defmodule DemoMapSets do

  @doc """
  This checks whether an element belongs to a mapset

      Examples
      iex> days = MapSet.new([:monday, :tuesday, :wednesday])
      #MapSet<[:monday, :tuesday, :wednesday]>
      iex> DemoMapSets.is_member?(days, :monday)
      true
      iex> DemoMapSets.is_member?(days, :sunday)
      false

  """
  @spec is_member?(MapSet.t(), any) :: boolean
  def is_member?(map_set, value) do
    MapSet.member?(map_set, value)
  end

  @doc """
  This function adds an element to the mapset

      Example
      iex> days = MapSet.new([:monday, :tuesday, :wednesday])
      iex> new_days = DemoMapSets.put(days, :thursday)
      iex> DemoMapSets.is_member?(new_days, :thursday)
      true

  """
  @spec put(MapSet.t(), any) :: MapSet.t()
  def put(map_set, value) do
    MapSet.put(map_set, value)
  end
end
