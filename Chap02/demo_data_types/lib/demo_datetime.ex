defmodule DemoDatetime do

  @doc """
  is_in_range?/3 verifies whether a given date is
  within a range between 2 predefined dates.

      Example
      iex> from_date = ~D[2001-01-01]; to_date = ~D[2002-01-01]
      iex> DemoDatetime.is_in_range?(from_date, to_date, ~D[2001-01-03])
      true

  """
  @spec is_in_range?(Date.t(), Date.t(), Date.t()) :: boolean
  def is_in_range?(from_date, to_date, date) when from_date <= to_date do
    range = get_range(from_date, to_date)
    date in range
  end
  def is_in_range?(_from_date, _to_date, _date) do
    false
  end

  defp get_range(from_date, to_date) do
    Date.range(from_date, to_date, 1)
  end
end
