defmodule DemoMaps do

  @type field() :: :name  | :age    | :works_at
  @type value() :: binary | integer | binary
  @type person() :: map

  @doc """
  This updates the person's map

      Example
      iex> person = %{age: 25, name: "Bob", works_at: "Initech"}
      person = %{age: 25, name: "Bob", works_at: "Initech"}
      iex> DemoMaps.update_person(person, :name, "Gradie")
      %{age: 25, name: "Gradie", works_at: "Initech"}

  """
  @spec update_person(person, field, value) :: map()
  def update_person(person, :name, value) do
    %{ person | name: value }
  end
  def update_person(person, :age, value) do
    %{ person | age: value }
  end
  def update_person(person, :works_at, value) do
    %{ person | works_at: value }
  end
end
