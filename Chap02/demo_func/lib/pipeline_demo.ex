defmodule PipelineDemo do

  @doc """
  Calcutes the absolute value of a number
  Then print

  """
  @spec print_abs_value(number) :: no_return
  def print_abs_value(value) when is_number(value) do
    value
    |> abs()
    |> Integer.to_string()
    |> IO.puts()
  end
end
