defmodule ArityDemo do

  @doc """
  This function calculates the sum of a number and zero

  ## Example

      iex> ArityDemo.sum(5)
      5
  """
  @spec sum(number) :: number
  def sum(value) do
    sum(value, 0)
  end

  defp sum(value_1, value_2) do
    value_1 + value_2
  end
end
