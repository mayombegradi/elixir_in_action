defmodule DefaultValueDemo do
  @doc """
  This function calculates the sum of two numbers

  ## Examples

      iex> DefaultValueDemo.sum(5)
      5
      iex> DefaultValueDemo.sum(5, 3)
      8
  """
  def sum(value_1, value_2 \\ 0) do
    value_1 + value_2
  end
end
