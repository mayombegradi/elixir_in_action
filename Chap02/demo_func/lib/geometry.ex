defmodule Geometry do

  @doc """
  Calculates the area of a square

  ## Example

      iex> Geometry.ssquare(5)
      25
  """
  @spec square(number) :: number
  def square(side) do
    rectangle_area_v1(side, side)
  end
  @doc """
  Calculates the area of a rectangle
  Using long function definition

  ## Example

      iex> Geometry.rectangle_area_v1(4, 5)
      20
  """
  @spec rectangle_area_v1(number, number) :: number
  def rectangle_area_v1(side_1, side_2) do
    side_1 * side_2
  end

  @doc """
  Calculates the area of a rectangle
  Using condensed function definition

  ## Example

      iex> Geometry.rectangle_area_v2(4, 5)
      20
  """
  @spec rectangle_area_v2(number, number) :: number
  def rectangle_area_v2(side_1, side_2), do: side_1 * side_2
end
