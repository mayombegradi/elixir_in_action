defmodule AliasDemo do
  alias Geometry.Rectangle

  @doc """
  Calculates the area of a rectangle

  ## Example

      iex> AliasDemo.area(4, 5)
      20
  """
  @spec area_rectangle(number, number) :: number
  def area_rectangle(side_1, side_2) do
    Rectangle.area(side_1, side_2)
  end
end
