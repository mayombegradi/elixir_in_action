defmodule AttributesDemo do
  @moduledoc """
  This module provides few module attributes examples.

  """
  @pi 3.14159

  @doc """
  Calculates the area of a circle

  ## Example

      iex> AttributesDemo.area(10)
      314.159
  """
  @spec area(number) :: number
  def area(radius), do: radius * radius * @pi

  @doc """
  Calculates the circumference of a circle

  ## Example

      iex> AttributesDemo.circumference(10)
      62.8318
  """
  @spec circumference(number) :: number
  def circumference(radius), do: 2 * radius * @pi
end
