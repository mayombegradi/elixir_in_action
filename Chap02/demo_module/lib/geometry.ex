defmodule Geometry do

  @doc """
  Calculates the area of a rectangle

  ## Example

      iex> Geometry.rectangle_area(4, 5)
      20
  """
  @spec rectangle_area(number, number) :: number
  def rectangle_area(side_1, side_2) do
    side_1 * side_2
  end

  defmodule Rectangle do
    @doc """
    Calculates the area of a rectangle

    ## Example

        iex> Geometry.Rectangle.area(4, 5)
        20
    """
    @spec area(number, number) :: number
    def area(side_1, side_2) do
      side_1 * side_2
    end
  end
end
