defmodule Todo.Metrics do
  use Task

  @spec start_link(any) :: {:ok, pid}
  def start_link(_arg) do
    Task.start_link(&loop/0)
  end

  defp loop() do
    Process.sleep(Application.fetch_env!(:todo_web, :todo_item_expiry))
    IO.inspect(collect_metrics())
    loop()
  end

  defp collect_metrics() do
    [
      memory_usage: :erlang.memory(:total),
      process_count: :erlang.system_info(:process_count)
    ]
  end
end
