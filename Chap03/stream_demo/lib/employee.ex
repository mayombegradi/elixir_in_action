defmodule Employee do

  @doc """
  Prints employees with their respective index.

  ## Example

      iex> Employee.print_employees(["Alice", "Bob", "John"])
      1. Alice
      2. Bob
      3. John
      :ok

  """
  @spec print_employees(list) :: no_return
  def print_employees(employees) do
    employees
    |> Stream.with_index()
    |> Enum.each(
      fn {employee, index} ->
        IO.puts("#{index+1}. #{employee}")
      end
    )
  end
end
