defmodule Files do

  @doc """
  A lines_lengths!/1 that takes a file path and returns a list of numbers, with
  each number representing the length of the corresponding line from the file.

  ## Example

      iex> Files.large_lines!("/home/ericson/Tut/Elixir/InAction/Chap03/stream_demo/mix.exs")
      ["      # {:dep_from_git, git: \"https://github.com/elixir-lang/my_dep.git\", tag: \"0.1.0\"}"]
  """
  @spec large_lines!(Path.t()) :: [integer]
  def large_lines!(path) do
    File.stream!(path)
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Enum.filter(&(String.length(&1) > 80))
  end

  @doc """
  A longest_line_length!/1 that returns the length of the longest line in a file.

  ## Example

      iex> Files.lines_lengths!("/home/ericson/Tut/Elixir/InAction/Chap03/stream_demo/mix.exs")
      [28, 17, 0, 16, 5, 17, 23, 24, 42, 18, 5, 5, 0, 59, 20, 5, 35, 5, 5, 0, 52, 14,
       5, 38, 87, 5, 5, 3]
  """
  @spec lines_lengths!(Path.t()) :: [integer]
  def lines_lengths!(path) do
    File.stream!(path)
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Stream.map(&String.length/1)
    |> Enum.to_list()
  end

  @doc """
  A longest_line!/1 that returns the contents of the longest line in a file.

  ## Example

      iex> Files.longest_line_length!("/home/ericson/Tut/Elixir/InAction/Chap03/stream_demo/mix.exs")
      {87, "      # {:dep_from_git, git: \"https://github.com/elixir-lang/my_dep.git\", tag: \"0.1.0\"}"}
  """
  @spec longest_line_length!(Path.t()) :: integer
  def longest_line_length!(path) do
    File.stream!(path)
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Stream.map(&{String.length(&1), &1})
    |> Enum.reduce({0, nil}, &longest_line_length/2)
  end

  @doc """
  A words_per_line!/1 that returns a list of numbers, with each number rep-
  resenting the word count in a file.

  ## Example

      iex> Files.words_per_line!("/home/ericson/Tut/Elixir/InAction/Chap03/stream_demo/mix.exs")
      [3, 2, 0, 3, 1, 2, 2, 3, 4, 2, 1, 1, 0, 9, 3,
       1, 2, 1, 1, 0, 9, 3, 1, 4, 6, 1, 1, 1]
  """
  @spec words_per_line!(Path.t()) :: [integer]
  def words_per_line!(path) do
    File.stream!(path)
    |> Stream.map(&String.replace(&1, "\n", ""))
    |> Stream.map(&String.split/1)
    |> Stream.map(&length/1)
    |> Enum.to_list()
  end

  defp longest_line_length({line_length, line_string}, {acc_length, acc_string}) do
    case line_length >= acc_length do
      true -> {line_length, line_string}
      false -> {acc_length, acc_string}
    end
  end
end
