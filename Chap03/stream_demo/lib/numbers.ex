defmodule Numbers do

  @spec print_square_roots(list) :: no_return
  def print_square_roots(numbers) do
    numbers
    |> Stream.filter(&(is_number(&1) and &1 > 0))
    |> Stream.map(&{&1, :math.sqrt(&1)})
    |> Stream.with_index()
    |> Enum.each(
      fn {{input, result}, index} ->
        IO.puts("#{index+1}. sqrt(#{input}) = #{result}")
      end
    )
  end
end
