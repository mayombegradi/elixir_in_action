defmodule Demo do
  @moduledoc """
  Documentation for `Demo`.
  """

  @doc """
  square/1 takes a list of numbers and raises them to the power of 2.

  ## Examples

      iex> Demo.square([1, 2, 3])
      [1, 4, 9]

      iex> Demo.square(1..3)
      [1, 4, 9]
  """
  @spec square(list | Range.t()) :: list
  def square(list) do
    for element <- list, do: element*element
  end

  @doc """
  table_of_mult_v1/0 retruns the table of multiplication of 3
  in a form of a list.

  ## Example

      iex> Demo.table_of_mult_v1()
      [
        {1, 1, 1}, {1, 2, 2}, {1, 3, 3},
        {2, 1, 2}, {2, 2, 4}, {2, 3, 6},
        {3, 1, 3}, {3, 2, 6}, {3, 3, 9}
      ]
  """
  @spec table_of_mult_v1() :: list
  def table_of_mult_v1() do
    for value_1 <- 1..3, value_2 <- :lists.seq(1, 3) do
      {value_1, value_2, value_1*value_2}
    end
  end

  @doc """
  table_of_mult_v2/0 returns the table of multiplication of 3
  in a map of the form %{{operand_1, operand_2} => result}

  ## Example

      iex> Demo.table_of_mult_v2()
      %{
          {1, 1} => 1, {1, 2} => 2, {1, 3} => 3,
          {2, 1} => 2, {2, 2} => 4, {2, 3} => 6,
          {3, 1} => 3, {3, 2} => 6, {3, 3} => 9
        }
  """
  @spec table_of_mult_v2() :: %{}
  def table_of_mult_v2() do
    for value_1 <- 1..3, value_2 <- 1..3, into: %{} do
      {{value_1, value_2}, value_1*value_2}
    end
  end
end
