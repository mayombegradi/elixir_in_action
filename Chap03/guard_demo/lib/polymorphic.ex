defmodule Polymorphic do

  @doc """
  Doubles a number or a string.

  ## Examples

      iex> Polymorphic.double(5)
      10

      iex> Polymorphic.double("hello")
      "hellohello"
  """
  @spec double(number | binary) :: number | binary
  def double(value) when is_number(value), do: 2 * value
  def double(value) when is_binary(value), do: value <> value
end
