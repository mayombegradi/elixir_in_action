defmodule Guards do

  @doc """
  `test` takes a number and tells whether it's positive, negative or zero.

  ## Examples
      iex> Guards.test(0)
      :zero

      iex> Guards.test(1)
      :positive

      iex> Guards.test(-1)
      :negative

  """
  @spec test(number) :: :zero | :positive | :negative
  def test(0), do: :zero
  def test(value) when is_number(value) and value > 0, do: :positive
  def test(value) when is_number(value) and value < 0, do: :negative
end
