defmodule Rec do

  @doc """
  list_len/1 returns the length of a given list.

  ## Example

      iex> Rec.list_len(:lists.seq(1, 10))
      10
  """
  @spec list_len(list) :: integer
  def list_len(list) when is_list(list) do
    list_len(list, 0)
  end

  @doc """
  range/2 creates a list from 2 integers, and
  whose content consists of numbers starting from
  the first element to the last one included.

  ## Example

      iex> Rec.range(1, 5)
      [1, 2, 3, 4, 5]
  """
  @spec range(integer, integer) :: list
  def range(first, last) when first <= last and
                              is_integer(first) and
                              is_integer(last) do
    range(first, last, [])
  end

  @doc """
  This calculates the sum of all the numbers contained in a list.

  ## Example

      iex> Rec.sum([1, "not a number", 2, :x, 3])
      6
  """
  @spec sum(list) :: number
  def sum(list) do
    Enum.reduce(list, 0, &sum/2)
  end

  defp sum(value, current_sum) when is_number(value), do: value+current_sum
  defp sum(_value, current_sum), do: current_sum

  defp list_len([], acc), do: acc
  defp list_len([_head|tail], acc), do: list_len(tail, acc+1)

  defp range(first, last, acc) when first == last, do: Enum.reverse([last|acc])
  defp range(first, last, acc), do: range(first+1, last, [first|acc])
end
