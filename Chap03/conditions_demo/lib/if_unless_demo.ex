defmodule IfUnlessDemo do

  @spec max_v1(number, number) :: number
  def max_v1(value_1, value_2) do
    if value_1 >= value_2 do
      IO.puts("max_v1(#{value_1}, #{value_2}) => #{value_1}")
      value_1
    else
      IO.puts("max_v1(#{value_1}, #{value_2}) => #{value_2}")
      value_2
    end
  end

  @spec max_v2(number, number) :: number
  def max_v2(value_1, value_2) do
    unless value_1 < value_2 do
      IO.puts("max_v1(#{value_1}, #{value_2}) => #{value_1}")
      value_1
    else
      IO.puts("max_v1(#{value_1}, #{value_2}) => #{value_2}")
      value_2
    end
  end
end
