defmodule WithDemo do

  @doc """
  Extracts user's details from a map.

  ## Examples
      iex> WithDemo.extract_user_v1(%{ name: "Bob", email: "bob@gmail.com", password: "&xi(2BO1bx'" })
      {:error, :login, :not_found}

      iex> WithDemo.extract_user_v1(%{ login: "Bob", email: "bob@gmail.com", password: "&xi(2BO1bx'" })
      {:ok, %{email: "bob@gmail.com", login: "Bob", password: "&xi(2BO1bx'"}}
  """
  @spec extract_user_v1(%{}) :: %{}
  def extract_user_v1(user) do
    with  {:ok, login} <- extract_user(:login, user),
          {:ok, email} <- extract_user(:email, user),
          {:ok, password} <- extract_user(:password, user) do
            {:ok, %{ login: login, email: email, password: password }}
    end
  end

  @doc """
  Extracts user's details from a map.

  ## Examples
      iex> WithDemo.extract_user_v2(%{ name: "Bob", email: "bob@gmail.com", password: "&xi(2BO1bx'" })
      {:error, :login, :not_found}

      iex> WithDemo.extract_user_v2(%{ login: "Bob", email: "bob@gmail.com", password: "&xi(2BO1bx'" })
      {:ok, %{email: "bob@gmail.com", login: "Bob", password: "&xi(2BO1bx'"}}
  """
  @spec extract_user_v2(%{}) :: %{}
  def extract_user_v2(user) do
    fields = [:login, :email, :password]
    case Enum.filter(fields, &(not Map.has_key?(user, &1))) do
      [] ->
        {:ok, %{ login: user[:login], email: user[:email], password: user[:password] }}
      [field|_tail] ->
        {:error, field, :not_found}
    end
  end

  defp extract_user(field, user) do
    case Map.fetch(user, field) do
      {:ok, value} -> {:ok, value}
      :error -> {:error, field, :not_found}
    end
  end
end
