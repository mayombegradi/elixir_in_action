defmodule CaseDemo do

  @spec max(number, number) :: number
  def max(value_1, value_2) do
    case value_1 >= value_2 do
      true  -> value_1
      false -> value_2
    end
  end
end
