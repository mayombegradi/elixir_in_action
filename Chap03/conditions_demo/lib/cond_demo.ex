defmodule CondDemo do

  @spec max(number, number) :: number
  def max(value_1, value_2) do
    cond do
      value_1 >= value_2 -> value_1
      true -> value_2
    end
  end
end
