defmodule PatternDemo do

  @doc """
  This function demonstrates binary pattern-matching.

  """
  @spec demo() :: no_return
  def demo() do
    "ping " <> url = "ping www.example.com"
    IO.puts(url)

    <<value1, value2, value3>> = "ABC"
    IO.puts("#{value1} - #{value2} - #{value3}")
  end
end
