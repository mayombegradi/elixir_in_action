defmodule Geomotry do
  @pi 3.14
  alias :math, as: Math

  @type side() :: number
  @type radius() :: number
  @type args() :: {:square, side} |
                  {:circle, radius} |
                  {:rectangle, side, side}

  @doc """
  Area function calculates the area of:
    * Square
    * Circle
    * Rectangle

  Based on the parameters passed.

  ## Examples
      iex> Geomotry.area({:square, 5})
      25.0

      iex> Geometry.area({:circle, 5})
      78.5

      iex> Geometry.area({:rectangle, 3, 5})
      15

  """
  @spec area(args) :: number
  def area({:square, side}), do: Math.pow(side, 2)
  def area({:circle, radius}), do: Math.pow(radius, 2) * @pi
  def area({:rectangle, side_1, side_2}), do: side_1 * side_2
end
